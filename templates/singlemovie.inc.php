<div class="row">
       <div class="col-xs-12">
         <ol class="breadcrumb">
         <li><a href="./">Home</a></li>
         <li><a href="./?page=movies">Movies</a></li>
         <li class="active"><?= $movie->title ?></li>
       </ol>
         <h1 style="text-align:center">Movie</h1>


           <h2> <?= $movie->title; ?></h2>
           <p>This movie was released in <?php echo $movie->year; ?></p>
           <p> <? echo $movie->description; ?></p>
           <?php if (static::$auth->isAdmin()): ?>
            <p>
            <a href="./?page=movie.edit&amp;id=<?= $movie->id ?>" class="btn btn-default">
              <span class="glyphicon glyphicon-pencil"></span> Edit Movie
            </a>
          </p>
        <?php endif ?>
           <img src="http://placehold.it/500x800?text=<?php echo $movie->title ?>" class="img-responsive" width="500px">

       </div>
     </div>
