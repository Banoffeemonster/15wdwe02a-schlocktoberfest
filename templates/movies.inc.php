   <div class="row">
        <div class="col-xs-12">
          <ol class="breadcrumb">
            <li><a href="./">Home</a></li>
            <li class="active">Movies</li>
          </ol>
          <h1>Movies</h1>

          <?php if (static::$auth->isAdmin()): ?>
            <p>
              <a href="./?page=movie.create" class="btn btn-default">
                <span class="glyphicon glyphicon-plus"></span> Add Movie
              </a>
            </p>
          <?php endif; ?>

          <?php if (count($movies) > 0): ?>

            <ul>
              <?php foreach($movies as $movie): ?>
                <li><a href="./?page=movie&amp;id=<?= $movie->id ?>">
                <?= $movie->title; ?> (<?= $movie->year; ?>)
                </a></li>
              <?php endforeach; ?>
            </ul>

          <?php else: ?>

            <p>Weirdly enough, there are no movies to show you. Spooky.</p>

          <?php endif; ?>

          <?php $this->paginate("./?page=movies&ps=" . $pageSize, $p, $recordCount, $pageSize, 5); ?>
          <div class="dropup">
            <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Films per page
            <span class="caret"></span></button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
              <li role="presentation"><a role="menuitem" tabindex="-1" href="./?page=movies&amp;ps=10">10 per page</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="./?page=movies&amp;ps=25">25 per page</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="./?page=movies&amp;ps=100">100 per page</a></li>
            </ul>
          </div>

        </div>
      </div>
