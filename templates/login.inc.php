<?php
    $errors = $user->errors;
?>
      <div class="row">
        <div class="col-xs-12">
          <form method="POST" action="./?page=auth.attempt" class="form-horizontal">
            <h1>Log In</h1>

            <?php if ($error): ?>
              <div class="alert alert-warning" role="alert">
                <strong>Error: invalid email or username.</strong>
                <p>Sorry, we don't have that email and password combination in our records. Please try again, or <a href="">register below</a>.</p>
                </div>

            <?php endif; ?>

            <div class="form-group form-group-lg<?php if ($errors['email']): ?> has-error <?php endif; ?>">
              <label for="email" class="col-sm-4 col-md-2 control-label">Email Address</label>
              <div class="col-sm-8 col-md-10">
                <input id="email" class="form-control input-lg" name="email"
                  placeholder="ash@s-mart.com"
                  value="<?= $user->email; ?>">
                <div class="help-block"><?= $errors['email']; ?></div>
              </div>
            </div>

            <div class="form-group form-group-lg<?php if ($errors['password']): ?> has-error <?php endif; ?>">
              <label for="password" class="col-sm-4 col-md-2 control-label">Password</label>
              <div class="col-sm-8 col-md-10">
                <input id="password" class="form-control input-lg" name="password" type="password">
                <div class="help-block"><?= $errors['password']; ?></div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-4 col-sm-10 col-md-offset-2 col-md-10">
                <button class="btn btn-success">
                  <span class="glyphicon glyphicon-ok"></span> Log In
                </button>
              </div>

            <?php if ($error): ?>
              <div class="row">
                  <div class="col-xs-12">
                    <form method="POST" action="./?page=auth.store" class="form-horizontal">
                      <h3>Register New User</h3>

                      <div class="form-group form-group-lg<?php if ($errors['email']): ?> has-error <?php endif; ?>">
                        <label for="email" class="col-sm-4 col-md-2 control-label">Email Address</label>
                        <div class="col-sm-8 col-md-10">
                          <input id="email" class="form-control input-lg" name="email"
                            placeholder="ash@s-mart.com"
                            value="<?= $user->email; ?>">
                          <div class="help-block"><?= $errors['email']; ?></div>
                        </div>
                      </div>

                      <div class="form-group form-group-lg<?php if ($errors['password']): ?> has-error <?php endif; ?>">
                        <label for="password" class="col-sm-4 col-md-2 control-label">Password</label>
                        <div class="col-sm-8 col-md-10">
                          <input id="password" class="form-control input-lg" name="password" type="password">
                          <div class="help-block"><?= $errors['password']; ?></div>
                        </div>
                      </div>

                      <div class="form-group form-group-lg<?php if ($errors['password2']): ?> has-error <?php endif; ?>">
                        <label for="password2" class="col-sm-4 col-md-2 control-label">Confirm Password</label>
                        <div class="col-sm-8 col-md-10">
                          <input id="password2" class="form-control input-lg" name="password2" type="password">
                          <div class="help-block"><?= $errors['password2']; ?></div>
                        </div>
                      </div>


                      <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-10 col-md-offset-2 col-md-10">
                          <button class="btn btn-success">
                            <span class="glyphicon glyphicon-ok"></span> Register
                          </button>
                        </div>
                      </div>
                    </form>

                  </div>
              </div>
            <?php endif; ?>

            </div>
          </form>

        </div>
      </div>
