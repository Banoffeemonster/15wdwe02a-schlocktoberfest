<?php

namespace App\Models;

class Movie extends DatabaseModel
{
    
    protected static $columns = ['id', 'title', 'year', 'description'];

    protected static $tableName = "movies";

    protected static $validationRules = [
        'title'       => 'minlength:1',
        'year'        => 'minlength:4,maxlength:4,numeric',
        'description' => 'minlength:10'
    ];
}
